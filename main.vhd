----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:54:09 11/04/2013 
-- Design Name: 
-- Module Name:    Modulo_2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Modulo_2 is
    
	 Port ( CLK		: in  STD_LOGIC;
           Reset	: in  STD_LOGIC;
           Out_2	: out  STD_LOGIC);

end Modulo_2;

architecture Behavioral of Modulo_2 is

	signal Contador 		: std_logic_vector(27 downto 0);	
	signal Out_2_tmp		: std_logic;
	constant Prescaler	: std_logic_vector(27 downto 0) := x"17D7840";

begin

	Divisor_Frecuencia : process(CLK, Reset)
	
	begin
	
		if(Reset = '0') then
		
			Contador		<= (others => '0');
			Out_2_tmp	<= '0';
			
		elsif(clk'event and clk = '1') then
		
			if (Contador = Prescaler) then
			
				Contador		<= (others => '0');
				Out_2_tmp	<= not(Out_2_tmp);
				
			else
	
				Contador <= Contador + 1;
				
			end if;
		
		end if;
	
	end process;
	
	Out_2		<= Out_2_tmp;	

end Behavioral;
